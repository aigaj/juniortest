<?php

/**
 * Make a connection to database.
 */
class Connection
{

    /**
     *
     * @param array $config Collects data for connection to database
     * @return \PDO
     */
    public static function make($config)
    {
        try {
            return new PDO(
                $config['connection'].';dbname='.$config['name'],
                $config['username'], $config['password'], $config['options']);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}