<?php

namespace App\Core;

class Request
{

    /**
     * Trims link from unnecessary parts
     * @return string Parse a URL and return its components.
     */
    public static function uri()
    {

        return trim(
            parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'
        );
    }

    /**
     *
     * @return string
     */
    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}