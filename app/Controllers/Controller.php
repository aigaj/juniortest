<?php

namespace App\Controllers;

use App\Models\ProductsModel;

/**
 * Initialising model
 */
class Controller
{

    function __construct()
    {
        $this->productsmodel = new ProductsModel();
        $this->helper        = new \App\Controllers\Helper();
    }
}